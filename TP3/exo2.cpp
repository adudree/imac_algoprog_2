#include "tp3.h"
#include <QApplication>
#include <time.h>

MainWindow* w = nullptr;
using std::size_t;


void binarySearchAll(Array& array, int toSearch, int& indexMin, int& indexMax)
{
    int start = 0;
    int end = array.size();
    int mid;
    bool isPresent = false;

    while (start < end) {
        mid = (start+end)/2;
        if (toSearch > array[mid]) {
            start = mid+1;
        }
        else if (toSearch < array[mid]){
            end = mid;
        }
        else {
            indexMin = mid;
            indexMax = mid;
            isPresent = true;

            // s'il y a plusieurs fois la valeur dans le tableau
            while (toSearch == array[mid]) {
                indexMax = mid;
                mid++;
            }
            break;
        }
    }
    // on teste la présence de la valeur
    if (!isPresent) {
        indexMin = indexMax = -1;
    }

}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 500;
	w = new BinarySearchAllWindow(binarySearchAll);
	w->show();

	return a.exec();
}
