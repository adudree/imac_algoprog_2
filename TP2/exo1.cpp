#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w = nullptr;

void selectionSort(Array& toSort){
	// selectionSort
    for (int i = 0; i < toSort.size(); i++) { // indice du tableau
        for (int j = i ; j < toSort.size(); j++) { // j parcourt le tableau à partir de i
            // si on trouve une valeur plus grande que celle de référence, on inverse
            if (toSort.get(i) > toSort.get(j)) {
                toSort.swap(i,j);
            }
        }
    }

}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(selectionSort); // window which display the behavior of the sort algorithm
    w->show();

	return a.exec();
}
