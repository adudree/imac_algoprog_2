#include <QApplication>
#include <time.h>
#include "tp2.h"

MainWindow* w=nullptr;

void merge(Array& first, Array& second, Array& result);

void splitAndMerge(Array& origin)
{
    // stop statement = condition + return (return stop the function even if it does not return anything)

    // initialisation
    int moitie = origin.size()/2;
    Array& first = w->newArray(moitie);
    Array& second = w->newArray(origin.size()-moitie);

    // split
    for (int i = 0; i < origin.size(); i++) {
        if (i<moitie) {
            first[i] = origin[i];
        }
        else {
            second[i-moitie] = origin[i];
        }

    }

    // recursiv splitAndMerge of lowerArray and greaterArray
    if (moitie > 1) {
        splitAndMerge(first);
    }
    if (origin.size()-moitie > 1) {
        splitAndMerge(second);
    }

    // merge
    merge(first, second, origin);

}

void merge(Array& first, Array& second, Array& result)
{
    int cursorFirst = 0;
    int cursorSecond = 0;

    for (int i=0; i<result.size(); i++) {
        if (cursorFirst ==  first.size() || cursorSecond == second.size()) {

            if (cursorFirst == first.size()) {
                result.set(i,second.get(cursorSecond));
                cursorSecond++;
            }
            else {
                result.set(i,first.get(cursorFirst));
                cursorFirst++;
            }
        }
        else {

            if (first.get(cursorFirst) <= second.get(cursorSecond)) {
                result.set(i,first.get(cursorFirst));
                cursorFirst++;
            }
            else {
                result.set(i,second.get(cursorSecond));
                cursorSecond++;
            }
        }
    }
}
void mergeSort(Array& toSort)
{
    splitAndMerge(toSort);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(mergeSort);
	w->show();

	return a.exec();
}
