#include <QApplication>
#include <time.h>
using namespace std;
#include "tp2.h"

MainWindow* w=nullptr;

void insertionSort(Array& toSort){
	Array& sorted=w->newArray(toSort.size());

    sorted.get(0) = toSort.get(0);
    int sizeSorted;
    bool test;

    for(int i=1; i<toSort.size(); i++){
        sizeSorted = 0;
        test = false;

        while(sizeSorted < sorted.size()-1 && !test){
            if(sorted.get(sizeSorted) > toSort.get(i)){
                sorted.insert(sizeSorted, toSort.get(i));
                test = true;
            }
            sizeSorted++;
        }

        if(!test){
            sorted.insert(i, toSort.get(i));
        }
    }

	toSort=sorted; // update the original array
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(insertionSort); // window which display the behavior of the sort algorithm
	w->show();

	return a.exec();
}
