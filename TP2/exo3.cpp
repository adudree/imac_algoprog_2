#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w=nullptr;


void bubbleSort(Array& toSort){
	// bubbleSort
    int count = 0;
    while (count < toSort.size()) {
        for (int j = 1; j < toSort.size(); j++) {
            if (toSort[j-1] > toSort[j]) {
                toSort.swap(j-1,j);
            }
        }
        count++;
    }
}



int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	uint elementCount=20;
	MainWindow::instruction_duration = 100;
	w = new TestMainWindow(bubbleSort);
	w->show();

	return a.exec();
}
