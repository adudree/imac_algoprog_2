#include "tp1.h"
#include <QApplication>
#include <time.h>
void exprcarre(Point * z){ // Fonction pour mettre l'expression au carré via identité remarquable
    z->x = pow(z->x, 2) - pow(z->y, 2); // x^2 -y^2 au carré comme i^2 = -1
    z->y = 2*z->x*z->y; // 2*x*y
}



int isMandelbrot(Point z, int n, Point point){
    // recursiv Mandelbrot
    if (n == 0) {
        return 0;
    }

    if (sqrt(z.x*z.x+z.y+z.y)>2) {
        return n;
    }
    else {
        float oldx = z.x;

        z.x = oldx*oldx - z.y*z.y + point.x;
        z.y = 2*oldx*z.y + point.y;

        return isMandelbrot(z, n-1, point);
    }


}


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow* w = new MandelbrotWindow(isMandelbrot);
    w->show();

    a.exec();
}



