#include <iostream>

using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
    Noeud* dernier;
    int nbNoeuds;
};

struct DynaTableau{
    int* donnees;
    int nbDonnees;
    int size;
};


void initialise(Liste* liste)
{
    // à l'initialisation, la liste est vide
    liste->premier = NULL;
    liste->dernier = NULL;
    liste->nbNoeuds = 0;
}

bool est_vide(const Liste* liste)
{
    // renvoie true si la liste est vide
    if(liste->premier == NULL) {
        return true;
    }
    else {
        return false;
    }
}

void ajoute(Liste* liste, int valeur)
{
    // création du noeud à ajouter
    Noeud * newNoeud = new Noeud;
    newNoeud->donnee = valeur;
    newNoeud->suivant = NULL;

    if (est_vide(liste)) {
        liste->premier = newNoeud;
        liste->dernier = newNoeud; // on ajoute un seul noeud, donc le dernier noeud est identique au premier
    }
    else {
        // le nouveau noeud devient le dernier
        liste->dernier->suivant = newNoeud;
        liste->dernier = newNoeud;
    }
    liste->nbNoeuds++;
}

void affiche(const Liste* liste)
{
    cout << "\nLISTE :" << endl;

    Noeud * temp = liste->premier;
    // on affiche tant que temp pointe vers quelque chose
    while (temp) {
        cout << temp->donnee << endl;
        temp = temp->suivant;
    }
}

int recupere(const Liste* liste, int n)
{
    int compteur = 0;
    Noeud * temp = liste->premier;

    // si la valeur n°n existe dans la liste
    if (n>0 && n<= liste->nbNoeuds) {
        // on parcourt la liste jusqu'à trouver la valeur correspondante
        while(compteur != n) {
            temp = temp->suivant;
            compteur ++;
        }
        return temp->donnee;
    }
    else {
        return -1;
    }
}

int cherche(const Liste* liste, int valeur)
{
    int compteur = 1;
    Noeud * temp = liste->premier;

    // on parcourt la liste tant que temp pointe vers quelque chose
    while(temp->donnee != valeur && temp != NULL) {
        temp = temp->suivant;
        compteur ++;
    }
    return compteur;
}

void stocke(Liste* liste, int n, int valeur)
{
    int compteur = 1;
    Noeud * temp = liste->premier;

    // on parcourt la liste tant que temp pointe vers quelque chose
    while(compteur != n && temp != NULL) {
        temp = temp->suivant;
        compteur ++;
    }
    temp->donnee = valeur;
}


void initialise(DynaTableau* tableau, int capacite)
{
    tableau->size = capacite;
    tableau->donnees = new int[capacite];
    tableau->nbDonnees = 0;
}

bool est_vide(const DynaTableau* tableau)
{
    // en fonction du nombre de données entrées
    if (tableau->nbDonnees != 0) {
        return false;
    }
    else {
        return true;
    }
}

void ajoute(DynaTableau* tableau, int valeur)
{
    // si le tableau est plein
    if (tableau->nbDonnees == tableau->size) {
        int * temp = new int[tableau->size+1];

        for (int i=0; i<tableau->size; i++) {
            temp[i] = tableau->donnees[i];
        }

        //on attribue les bonnes valeurs aux bons endroits
        temp[tableau->size] = valeur;
        tableau->donnees = temp;

        tableau->size++;
    }
    else {
        // sinon, la première case libre prend la valeur
        tableau->donnees[tableau->nbDonnees] = valeur;
    }
    tableau->nbDonnees++;
}

void affiche(const DynaTableau* tableau)
{
    cout << "\nTABLEAU :" << endl;
    for (int i=0; i<tableau->nbDonnees; i++) {
        cout << tableau->donnees[i] << endl;
    }
}

int recupere(const DynaTableau* tableau, int n)
{
    if (n >0 && n < tableau->size) {
        return tableau->donnees[n];
    }
    return 0;
}

int cherche(const DynaTableau* tableau, int valeur)
{
    for (int i=0; i<tableau->nbDonnees; i++) {
        if (tableau->donnees[i] == valeur) {
            return i+1; // décalage d'indice
        }
    }
    return -1;
}

void stocke(DynaTableau* tableau, int n, int valeur)
{
    if (n >=0 && n < tableau->size) {
        tableau->donnees[n-1] = valeur; // décalage d'indice
    }
}

//void pousse_file(Liste* liste, int valeur)
void pousse_file(Liste* liste, int valeur)
{
    ajoute(liste, valeur);
}

//int retire_file(Liste* liste)
int retire_file(Liste* liste)
{
    // la première valeur entrée dans la liste est retournée
    int valeurToReturn = liste->premier->donnee;
    liste->premier = liste->premier->suivant;
    return valeurToReturn;
}

//void pousse_pile(Liste* liste, int valeur)
void pousse_pile(Liste* liste, int valeur)
{
    Noeud * newNoeud = new Noeud;

    // le nouveau noeud prend la place du premier noeud de la liste
    newNoeud->donnee = valeur;
    newNoeud->suivant = liste->premier;
    liste->premier = newNoeud;
}

//int retire_pile(Liste* liste)
int retire_pile(Liste* liste)
{
    // même fonction que retire_file (sauf qu'il s'agit cette fois de la dernière valeur entrée)
    int valeurToReturn = liste->premier->donnee;
    liste->premier = liste->premier->suivant;
    return valeurToReturn;
}

int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
    {
        std::cout << "Ma liste n'est pas vide." << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Mon tableau n'est pas vide" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Ma liste est vide." << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Mon tableau est vide." << std::endl;
    }

    std::cout << "Elements initiaux :" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans le tableau à " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7 (position 4): " << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    Liste pile; // DynaTableau pile;
    Liste file; // DynaTableau file;

    initialise(&pile);
    initialise(&file);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    compteur = 10;
    while(!est_vide(&file) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}
