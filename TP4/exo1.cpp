#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex)
{
    return nodeIndex*2+1;
}

int Heap::rightChild(int nodeIndex)
{
    return nodeIndex*2+2;
}

void Heap::insertHeapNode(int heapSize, int value)
{
    // use (*this)[i] or this->get(i) to get a value at index i
    int i = heapSize;
    this->get(i) = value;
    while (i > 0 && this->get(i) > this->get((i-1)/2)) {
        swap(i, (i-1)/2);
        i=(i-1)/2;
    }
}

void Heap::heapify(int heapSize, int nodeIndex)
{
    int i_max = nodeIndex;

    int largestIndex = i_max;

    int left = leftChild(i_max);
    int right = rightChild(i_max);

    if (left < heapSize && this->get(largestIndex) < this->get(left)) {
        largestIndex = left;
    }
    if (right < heapSize && this->get(largestIndex) < this->get(right)) {
        largestIndex = right;
    }

    if (this->get(largestIndex) != this->get(i_max)) {
        swap(i_max, largestIndex);
        heapify(heapSize, largestIndex);
    }
}

void Heap::buildHeap(Array& numbers)
{
    for (int i = 0; i < int(numbers.size()); i++) {
        insertHeapNode(i, numbers[i]);
    }
}

void Heap::heapSort()
{
    Array& sorted = w->newArray(this->size());
    for (int i = 0; i < int(this->size()); i++) {
        for (int j = i ; j < int(this->size()); j++) {
            if (this->get(i) > this->get(j)) {
                swap(i,j);
            }
            sorted[i] = this->get(i);
        }
    }
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
    w->show();

    return a.exec();
}
